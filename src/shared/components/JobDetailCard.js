import React from 'react';

const JobDetailCard = ({
  job: {
    degree,
    job_title,
    job_type,
    salary_range_from,
    salary_range_to,
    company_name,
    company_location,
    xp_lvl,
    created_at,
    company_logo
  }
}) => {
  // Update salary range using multiple of thousands
  const getSalaryRange = () => {
    const salaryMin = salary_range_from / 1000;
    const salaryMax = salary_range_to / 1000;
    const currency = '₱';
    return currency + salaryMin + 'k - ' + currency + salaryMax + 'k';
  };

  // Show the creation date using seconds / minutes / hours / days ago
  const getCreationTime = () => {
    const creationTime = new Date(created_at).getTime();
    const timeDiff = new Date().getTime() - creationTime;
    if (timeDiff < 60000) return 'few seconds ago';
    else if (timeDiff < 3600000) {
      const minutes = Math.floor(timeDiff / 60000);
      return minutes === 1 ? minutes + ' minute ago' : minutes + ' minutes ago';
    } else if (timeDiff < 86400000) {
      const hours = Math.floor(timeDiff / 3600000);
      return hours === 1 ? hours + ' hour ago' : hours + ' hours ago';
    } else {
      const days = Math.floor(timeDiff / 86400000);
      return days === 1 ? days + ' day ago' : days + ' days ago';
    }
  };

  return (
    <div className="job-wrapper">
      <div className="job-title">{job_title}</div>
      <div className="salary-range">{getSalaryRange()}</div>
      <div className="job-details">
        <div className="job-location">
          <span className="fas fa-map-marker-alt job-icon" /> {company_location}
        </div>
        <div className="job-experience">
          <span className="fas fa-briefcase job-icon" /> {xp_lvl}
        </div>
        <div className="degree">
          <span className="fas fa-graduation-cap job-icon" /> {degree}
        </div>
        <div className="job-type">
          <span className="fas fa-clock job-icon" /> {job_type}
        </div>
      </div>
      <div className="company-details">
        <img src={company_logo} alt="" width="40" height="40" />
        <p>{company_name}</p>
      </div>
      <div className="job-creation-date">{getCreationTime()}</div>
    </div>
  );
};

export default JobDetailCard;
