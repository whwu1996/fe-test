import React from 'react';

const LoadingScreen = () => {
  return (
    <img
      src={require('../../assets/images/loading-spinner.svg')}
      alt="loading"
      style={{ margin: '50px 80px 20px 155px' }}
    />
  );
};

export default LoadingScreen;
