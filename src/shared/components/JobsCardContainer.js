import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchJobs } from '../../actions/index';
import JobDetailCard from './JobDetailCard';
import LoadingScreen from './LoadingScreen';

class JobsCardContainer extends Component {
  componentDidMount() {
    this.props.fetchJobs();
  }

  render() {
    // Count total number of jobs returned
    const jobsCount = (
      <div className="jobs-count">
        {this.props.count > 1
          ? this.props.count + ' jobs found'
          : this.props.count + ' job found'}
      </div>
    );

    // Map through the list of jobs and make every job a single card component
    const jobList = this.props.jobs.map((job, index) => (
      <JobDetailCard key={index} job={job} />
    ));

    // Return loading screen is its still loading, else return job list
    return this.props.loading ? (
      <LoadingScreen />
    ) : (
      <div>
        {jobsCount} {jobList}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  jobs: state.jobs,
  loading: state.loading,
  count: state.count
});

const mapDispatchToProps = {
  fetchJobs: fetchJobs
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobsCardContainer);
