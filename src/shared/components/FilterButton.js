import React from 'react';

const button = {
  marginTop: '24px',
  border: '1px solid #3683eb',
  borderRadius: '25px',
  backgroundColor: '#F9F9F9',
  width: '100%',
  cursor: 'pointer'
};

const paragraph = {
  fontSize: '16px',
  fontWeight: 'bold',
  color: '#3683eb',
  margin: '6px',
  textAlign: 'center'
};

const FilterButton = () => (
  <button style={button}>
    <p style={paragraph}>Filter results</p>
  </button>
);

export default FilterButton;
