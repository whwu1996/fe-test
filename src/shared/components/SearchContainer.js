import React, { Component } from 'react';
import FilterButton from './FilterButton';
import { connect } from 'react-redux';
import { changeQuery, fetchJobs } from '../../actions/index';

class FilterContainer extends Component {
  constructor() {
    super();
    this.state = {
      query: ''
    };
  }

  timer = null;

  // Update query during onChange event
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.props.changeQuery(this.state.query);
      this.props.fetchJobs();
    }, 100);
  };

  // Trigger change when ENTER key is entered
  handleKeyDown = e => {
    if (e.keyCode === 13) {
      clearTimeout(this.timer);
      this.triggerChange();
    }
  };

  render() {
    return (
      <div className="search-container">
        <div>
          <span className="fas fa-search job-icon" /> Search for job title or
          company name
        </div>
        <div>
          <input
            type="text"
            name="query"
            value={this.state.query}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
          />
        </div>
        <FilterButton />
      </div>
    );
  }
}

const mapDispatchToProps = {
  changeQuery: changeQuery,
  fetchJobs: fetchJobs
};

export default connect(
  null,
  mapDispatchToProps
)(FilterContainer);
