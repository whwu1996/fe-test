import React, { Component } from 'react';
import './App.css';

import Header from './shared/components/Header';
import JobsCardContainer from './shared/components/JobsCardContainer';
import SearchContainer from './shared/components/SearchContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-Wrapper">
          <Header />
          <SearchContainer />
          <JobsCardContainer />
        </div>
      </div>
    );
  }
}

export default App;
