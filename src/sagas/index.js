import { put, takeLatest, all, select } from 'redux-saga/effects';

function* fetchJobs() {
  const query = yield select(state => state.query);

  const items = yield fetch(
    `https://search.bossjob.com/api/v1/search/job_filter?size=12&query=${query}`
  ).then(response => response.json());

  yield put({
    type: 'FETCH_JOBS_COMPLETED',
    jobs: items.data.jobs,
    count: items.data.total_num
  });
}

function* actionWatcher() {
  yield takeLatest('FETCH_ALL_JOBS', fetchJobs);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
