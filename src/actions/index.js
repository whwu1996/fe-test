export const fetchJobs = () => ({
  type: 'FETCH_ALL_JOBS'
});

export const changeQuery = query => ({
  type: 'CHANGE_QUERY',
  query
});
