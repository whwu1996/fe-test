const initialState = {
  query: '',
  jobs: [],
  count: '0'
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_ALL_JOBS':
      return { ...state, loading: true };
    case 'FETCH_JOBS_COMPLETED':
      return {
        ...state,
        jobs: action.jobs,
        count: action.count,
        loading: false
      };
    case 'CHANGE_QUERY':
      return {
        ...state,
        query: action.query
      };
    default:
      return state;
  }
};
export default reducer;
